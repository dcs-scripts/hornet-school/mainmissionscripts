-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Loading Airboss Script. Mission Version : V4.6.1 . Script Version : v4.2", 15, "INFO"):ToAll()

GeorgeWashington_Group = NAVYGROUP:New("GeorgeWashington") -- Group for the George Washington, using a previous variable to avoid confusion.
GeorgeWashington_Group:Activate()

-- Invincible_Group = NAVYGROUP:New("Invincible")
-- Invincible_Group:Activate()


-- Stennis_Group = NAVYGROUP:New("Stennis")
-- Stennis_Group:Activate()

-- Tarawa_Group = NAVYGROUP:New("Tarawa")
-- Tarawa_Group:Activate()


global_time_recovery_end = 0

ship_dictionary = {GeorgeWashington = {NavyGroup = GeorgeWashington_Group, ShipCallsign = "Warfighter", TimeRecoveryEnd=0} }
                   -- Stennis = {NavyGroup = Stennis_Group, ShipCallsign = "Courage", TimeRecoveryEnd=0} }
                   -- Invincible = {NavyGroup = Invincible_Group, ShipCallsign = "Invincible", TimeRecoveryEnd=0},
                   -- Tarawa = {NavyGroup = Tarawa_Group, ShipCallsign = "Tarawa", TimeRecoveryEnd=0} }

-----------------------------------------------------------------------------------------------------------------------------------------
--GWA Beacon and Lights
-----------------------------------------------------------------------------------------------------------------------------------------

-- GWA CVN Beacons and Lights START
-- Flags TCN = 100,101,
-- Flags ICLS = 102,103,
-- Flags Link 4 = 104,105,
-- Flags ACLS = 106,107,
-- Flags Lights = 108,109,110,111,112

gwa_messages = {TCN = {OFF="GWA TACAN Deactivated", ON="GWA TACAN Restarted"},
                ICLS = {OFF="GWA ICLS Deactivated", ON="GWA ICLS Restarted"},
                LINK4 = {OFF="GWA Link 4 Deactivated", ON="GWA Link 4 Restarted"},
                ACLS = {OFF="GWA ACLS Deactivated", ON="GWA ACLS Restarted"},
                LIGHTS = {OFF="GWA Lights off", AUTO="GWA Lights set to Auto", NAVIGATION="GWA Lights set to Navigation", LAUNCH="GWA Lights set to Launch", RECOVERY="GWA Lights set to Recovery"}}

gwa_flags = {TCN = {OFF="101", ON="11"},
             ICLS = {OFF="12", ON="13"},
             LINK4 = {OFF="14", ON="15"},
             ACLS = {OFF="16", ON="17"},
             LIGHTS = {OFF="18", AUTO="19", NAVIGATION="20", LAUNCH="21", RECOVERY="22"},
             MESSAGES = gwa_messages}
--[[
-----------------------------------------------------------------------------------------------------------------------------------------
--R05 Beacon and Lights
-----------------------------------------------------------------------------------------------------------------------------------------

-- R05 CVN Beacons and Lights START
-- Flags TCN = 200,201
-- Flags Lights = 202,203,204,205,206

r05_messages = {TCN = {OFF="R05 TACAN Deactivated"}, ON={"R05 TACAN Restarted"},
                LIGHTS = {OFF="R05 Lights Off", AUTO="R05 Lights set to Auto", NAVIGATION="R05 Lights set to Navigation", LAUNCH="R05 Lights set to Launch", RECOVERY="R05 Lights set to recovery"}}

r05_flags = {TCN = {OFF="30", ON="31"},
             LIGHTS = {OFF="32", AUTO="33", NAVIGATION="34", LAUNCH="35", RECOVERY="36"},
             MESSAGES = r05_messages}
--]]

---------------------------------------------
-- Stennis Beacon and Lights

-- Flags TCN = 400, 401
-- Flags ICLS = 402, 403
-- Flags Link 4 = 404, 405
-- Flags ACLs = 406, 407
-- Flags Lights = 408, 409, 410, 411, 412
---------------------------------------------

stennis_messages = {TCN = {OFF="STN TACAN Deactivated", ON="STN TACAN Restarted"},
                ICLS = {OFF="STN ICLS Deactivated", ON="STN ICLS Restarted"},
                LINK4 = {OFF="STN Link 4 Deactivated", ON="STN Link 4 Restarted"},
                ACLS = {OFF="STN ACLS Deactivated", ON="STN ACLS Restarted"},
                LIGHTS = {OFF="STN Lights Off", AUTO="STN Lights set to Auto", NAVIGATION="STN Lights set to Navigation", LAUNCH="STN Lights set to Launch", RECOVERY="STN Lights set to recovery"}}

stennis_flags = {TCN = {OFF="400", ON="401"},
             ICLS = {OFF="402", ON="403"},
             LINK4 = {OFF="404", ON="405"},
             ACLS = {OFF="406", ON="407"},
             LIGHTS = {OFF="408", AUTO="409", NAVIGATION="410", LAUNCH="411", RECOVERY="412"},
             MESSAGES = stennis_messages}
--[[
---------------------------------------------
-- Tarawa Beacon and Lights

-- Flags TCN = 300, 301
-- Flags ICLS = 302, 303
-- Flags Lights = 304, 305, 306, 307, 308
---------------------------------------------

tarawa_messages = {TCN = {OFF="LHA-1 TACAN Deactivated", ON="LHA-1 TACAN Restarted"},
                   ICLS = {OFF="LHA-1 ICLS Deactivated", ON="LHA-1 TACAN Restarted"},
                   LIGHTS = {OFF="LHA-1 Lights Off", AUTO="LHA-1 Lights set to Auto", NAVIGATION="LHA-1 Lights set to Navigation", LAUNCH="LHA-1 Lights set to Launch", RECOVERY="LHA-1 Lights set to recovery"}}

tarawa_flags = {TCN = {OFF="500", ON="501"},
                ICLS = {OFF="502", ON="503"},
                LIGHTS = {OFF="504", AUTO="505", NAVIGATION="506", LAUNCH="507", RECOVERY="508"},
                MESSAGES = tarawa_messages}
]]--

-----------------------------------------------------------------------------------------------------------------------------------------
--Airboss Menu functions
-----------------------------------------------------------------------------------------------------------------------------------------


function stopRecovery(ship_name)
  ship_dictionary[ship_name]["NavyGroup"]:TurnIntoWindStop()
  ship_dictionary[ship_name]["TimeRecoveryEnd"] = 0

  MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." recovery opperations complete, returning to base course", 15, "INFO"):ToAll()
end

function getTimeRemaining(ship_name)

   local steaming_into_wind = ship_dictionary[ship_name]["NavyGroup"]:IsSteamingIntoWind()

   if not (steaming_into_wind) then
      MESSAGE:New("Not steaming into the wind", 10, "INFO"):ToAll()
   else
      local time_now = timer.getAbsTime()
      timer_recovery_end = UTILS.ClockToSeconds(ship_dictionary[ship_name]["TimeRecoveryEnd"])

      local time_left = timer_recovery_end - time_now

      local time_left_hours = math.floor(time_left / 3600)
      local time_left_min = math.floor(time_left % 3600 / 60)
      local time_left_sec = math.floor(time_left % 3600 % 60)

      MESSAGE:New("Time till turn to base recovery course (HH:MM) "..time_left_hours..":"..time_left_min, 15, "INFO"):ToAll()
   end
end

function startRecovery(recovery_parameters)

   local recovery_time = recovery_parameters[1]
   local ship_name = recovery_parameters[2]

   local time_now = timer.getAbsTime()
   local time_end = time_now + recovery_time*60

   local steaming_into_wind = ship_dictionary[ship_name]["NavyGroup"]:IsSteamingIntoWind()

   if (steaming_into_wind) then
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." is already steaming into the wind till "..ship_dictionary[ship_name]["TimeRecoveryEnd"], 15, "INFO"):ToAll()
   else
      local timer_recovery_start = UTILS.SecondsToClock(time_now, false)
      timer_recovery_end = UTILS.SecondsToClock(time_end, false)
      ship_dictionary[ship_name]["TimeRecoveryEnd"] = timer_recovery_end

      ship_dictionary[ship_name]["NavyGroup"]:AddTurnIntoWind(timer_recovery_start, timer_recovery_end, 35, false, -9.1)
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." turning, at time "..timer_recovery_start.." until "..timer_recovery_end, 15, "INFO"):ToAll()
   end
end

function checkSteamingIntoWind(ship_name)
   local steaming_into_wind = ship_dictionary[ship_name]["NavyGroup"]:IsSteamingIntoWind()

   if (steaming_into_wind) then
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." is steaming into the wind.", 15, "INFO"):ToAll()
   else
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].."r is on normal course.", 15, "INFO"):ToAll()
   end
end

function beaconsLightsFlag(ship_dict_beacon_flag)

   local ship_dict     = ship_dict_beacon_flag[1]
   local ship_beacon   = ship_dict_beacon_flag[2]
   local beacon_flag   = ship_dict_beacon_flag[3]

   local beacon_on = USERFLAG:New(ship_dict[ship_beacon][beacon_flag]):Set(true)

   MESSAGE:New(ship_dict["MESSAGES"][ship_beacon][beacon_flag], 15, "INFO"):ToAll()
end

MESSAGE:New("Loading Menu Options for the Carriers and LHAs", 15, "INFO"):ToAll()

--Main Menu
TopMenu1 = MENU_COALITION:New(coalition.side.BLUE, "Carrier Menus" )

-----------------------------------------------------------------------------------------------------------------------------------------
--GWA Menu 
-----------------------------------------------------------------------------------------------------------------------------------------

--Level 1
gwa_menu = MENU_COALITION:New(coalition.side.BLUE, "GWA" , TopMenu1)

--Level 2

gwa_course = MENU_COALITION:New(coalition.side.BLUE, "Turn into the Wind" , gwa_menu)
gwa_beacon = MENU_COALITION:New(coalition.side.BLUE, "Cycle CVN Beacons" , gwa_menu)
gwa_lights = MENU_COALITION:New(coalition.side.BLUE, "Cycle CVN Lights" , gwa_menu)

--Level 3

gwa_base_course = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Stop Recovery and return to base course", gwa_course, stopRecovery, "GeorgeWashington")
Menu1112 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 30 minutes", gwa_course, startRecovery, {30, "GeorgeWashington"})
Menu1113 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 60 minutes", gwa_course, startRecovery, {60, "GeorgeWashington"})
Menu1114 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 90 minutes", gwa_course, startRecovery, {90, "GeorgeWashington"})
Menu1116 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Check for steaming into wind", gwa_menu, checkSteamingIntoWind, "GeorgeWashington")
Menu1117 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Get remaining time for recovery", gwa_menu, getTimeRemaining, "GeorgeWashington")

gwa_tacan = MENU_COALITION:New(coalition.side.BLUE, "TACAN" , gwa_beacon) -- Menu1121
gwa_icls = MENU_COALITION:New(coalition.side.BLUE, "ICLS" , gwa_beacon) -- Menu 1122
gwa_link4 = MENU_COALITION:New(coalition.side.BLUE, "LINK 4" , gwa_beacon) -- Menu 1123
gwa_acls = MENU_COALITION:New(coalition.side.BLUE, "ACLS" , gwa_beacon) -- Menu 1124

gwa_lights_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Turn Off CVN Lights", gwa_lights, beaconsLightsFlag, {gwa_flags, "LIGHTS", "OFF"})
gwa_lights_auto = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights to Auto", gwa_lights, beaconsLightsFlag, {gwa_flags, "LIGHTS", "AUTO"})
gwa_lights_navigation = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights to Navigation", gwa_lights, beaconsLightsFlag, {gwa_flags, "LIGHTS", "NAVIGATION"})
gwa_lights_launch = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights for Launch", gwa_lights, beaconsLightsFlag, {gwa_flags, "LIGHTS", "LAUNCH"})
gwa_lights_reconvery = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights for Recovery", gwa_lights, beaconsLightsFlag, {gwa_flags, "LIGHTS", "RECOVERY"})

--Level 4
gwa_tacan_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate TACAN", gwa_tacan, beaconsLightsFlag, {gwa_flags, "TCN", "OFF"})
gwa_tacan_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart TACAN", gwa_tacan, beaconsLightsFlag, {gwa_flags, "TCN", "ON"})
gwa_icls_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate ICLS", gwa_icls, beaconsLightsFlag, {gwa_flags, "ICLS", "OFF"})
gwa_icls_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart ICLS", gwa_icls, beaconsLightsFlag, {gwa_flags, "ICLS", "ON"})
gwa_link4_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate LINK 4", gwa_link4, beaconsLightsFlag, {gwa_flags, "LINK4", "OFF"})
gwa_lin4_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart LINK 4", gwa_link4, beaconsLightsFlag, {gwa_flags, "LINK4","ON"})
gwa_acls_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate ACLS", gwa_acls, beaconsLightsFlag, {gwa_flags, "ACLS", "OFF"})
gwa_acls_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart ACLS", gwa_acls, beaconsLightsFlag, {gwa_flags, "ACLS", "ON"})

-- GWA End

--[[
-----------------------------------------------------------------------------------------------------------------------------------------
--R05 Menu
-----------------------------------------------------------------------------------------------------------------------------------------

-- Level 1
invincible_menu = MENU_COALITION:New(coalition.side.BLUE, "R05", TopMenu1)

-- Level 2

invincible_course = MENU_COALITION:New(coalition.side.BLUE, "Turn into the wind", invincible_menu) -- Course Changes
invincible_beacons = MENU_COALITION:New(coalition.side.BLUE, "Cycle R05 Beacons", invincible_menu) -- Beacons
invincible_lights = MENU_COALITION:New(coalition.side.BLUE, "Cycle R05 Lights", invincible_menu) -- Lights

-- Level 3
-- Course Changes R05
invincible_base_course = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Stop Recovery and return to base course", invincible_course, stopRecovery, "Invincible")
invincible_wind_30 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Request R05 turn into the wind for 30 minutes", invincible_course, startRecovery, {30, "Invincible"})
invincible_wind_60 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Request R05 turn into the wind for 60 minutes", invincible_course, startRecovery, {60, "Invincible"})
invincible_wind_90 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Request R05 turn into the wind for 90 minutes", invincible_course, startRecovery, {90, "Invincible"})
invincible_check_wind = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Check for steaming into wind", invincible_menu, checkSteamingIntoWind, "Invincible")
invincible_recovery_time = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Get remaining time for recovery", invincible_menu, getTimeRemaining, "Invincible")

-- Beacons
invincible_tcn = MENU_COALITION:New(coalition.side.BLUE, "TACAN", invincible_beacons)
invincible_tcn_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Deactivate TACAN", invincible_tcn, beaconsLightsFlag, {r05_flags, "TCN", "OFF"})
invincible_tcn_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Restart TACAN", invincible_tcn, beaconsLightsFlag, {r05_flags, "TCN", "ON"})

-- Lights
invincible_lights_off_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Turn off R05 Lights", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "OFF"})
invincible_lights_auto_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "R05 Lights to Auto", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "AUTO"})
invincible_lights_navigation_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "R05 Lights to Navigation", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "NAVIGATION"})
invincible_lights_launch_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "R05 Lights to Launch", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "LAUNCH"})
invincible_lights_recovery_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "R05 Lights to Recovery", invincible_lights, beaconsLightsFlag, {r05_flags, "LIGHTS", "RECOVERY"})

-- R05 End
]]--

--[[
---------------------------------------------
-- STN Menu
---------------------------------------------

-- Level 1
stennis_menu = MENU_COALITION:New(coalition.side.BLUE, "STN", TopMenu1)

-- Level 2
stennis_course = MENU_COALITION:New(coalition.side.BLUE, "Turn into the wind", stennis_menu)
stennis_beacons = MENU_COALITION:New(coalition.side.BLUE, "Cycle STN Beacons", stennis_menu)
stennis_lights = MENU_COALITION:New(coalition.side.BLUE, "Cycle STN Lights", stennis_menu)

-- Level 3 Course
stennis_base_course = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Stop Recovery and return to base course", stennis_course, stopRecovery, "Stennis")
Menu1112 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 30 minutes", stennis_course, startRecovery, {30, "Stennis"})
Menu1113 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 60 minutes", stennis_course, startRecovery, {60, "Stennis"})
Menu1114 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 90 minutes", stennis_course, startRecovery, {90, "Stennis"})
Menu1116 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Check for steaming into wind", stennis_menu, checkSteamingIntoWind, "Stennis")
Menu1117 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Get remaining time for recovery", stennis_menu, getTimeRemaining, "Stennis")

-- Level 3 Beacons STN
stennis_tacan = MENU_COALITION:New(coalition.side.BLUE, "TACAN" , stennis_beacons) -- Menu1121
stennis_icls = MENU_COALITION:New(coalition.side.BLUE, "ICLS" , stennis_beacons) -- Menu 1122
stennis_link4 = MENU_COALITION:New(coalition.side.BLUE, "LINK 4" , stennis_beacons) -- Menu 1123
stennis_acls = MENU_COALITION:New(coalition.side.BLUE, "ACLS" , stennis_beacons) -- Menu 1124

-- Level 3 Lights STN
stennis_lights_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Turn Off CVN Lights", stennis_lights, beaconsLightsFlag, {stennis_flags, "LIGHTS", "OFF"})
stennis_lights_auto = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights to Auto", stennis_lights, beaconsLightsFlag, {stennis_flags, "LIGHTS", "AUTO"})
stennis_lights_navigation = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights to Navigation", stennis_lights, beaconsLightsFlag, {stennis_flags, "LIGHTS", "NAVIGATION"})
stennis_lights_launch = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights for Launch", stennis_lights, beaconsLightsFlag, {stennis_flags, "LIGHTS", "LAUNCH"})
stennis_lights_reconvery = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights for Recovery", stennis_lights, beaconsLightsFlag, {stennis_flags, "LIGHTS", "RECOVERY"})

--Level 4
stennis_tacan_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate TACAN", stennis_tacan, beaconsLightsFlag, {stennis_flags, "TCN", "OFF"})
stennis_tacan_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart TACAN", stennis_tacan, beaconsLightsFlag, {stennis_flags, "TCN", "ON"})
stennis_icls_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate ICLS", stennis_icls, beaconsLightsFlag, {stennis_flags, "ICLS", "OFF"})
stennis_icls_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart ICLS", stennis_icls, beaconsLightsFlag, {stennis_flags, "ICLS", "ON"})
stennis_link4_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate LINK 4", stennis_link4, beaconsLightsFlag, {stennis_flags, "LINK4", "OFF"})
stennis_lin4_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart LINK 4", stennis_link4, beaconsLightsFlag, {stennis_flags, "LINK4","ON"})
stennis_acls_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate ACLS", stennis_acls, beaconsLightsFlag, {stennis_flags, "ACLS", "OFF"})
stennis_acls_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart ACLS", stennis_acls, beaconsLightsFlag, {stennis_flags, "ACLS", "ON"})

-- STN End
--]]


--[[
---------------------------------------------
-- LHA-1 Tarawa Start
---------------------------------------------

-- Level 1
tarawa_menu = MENU_COALITION:New(coalition.side.BLUE, "LHA-1", TopMenu1)

-- Level 2
tarawa_course = MENU_COALITION:New(coalition.side.BLUE, "Turn into the wind", tarawa_menu)
tarawa_beacons = MENU_COALITION:New(coalition.side.BLUE, "Cycle LHA-1 Beacons", tarawa_menu)
tarawa_lights = MENU_COALITION:New(coalition.side.BLUE, "Cycle LHA-1 Lights", tarawa_menu)

-- Level 3 Course
tarawa_base_course = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Stop Recovery and return to base course", tarawa_course, stopRecovery, "Tarawa")
Menu1112 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 30 minutes", tarawa_course, startRecovery, {30, "Tarawa"})
Menu1113 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 60 minutes", tarawa_course, startRecovery, {60, "Tarawa"})
Menu1114 = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Request CVN turn into the wind for 90 minutes", tarawa_course, startRecovery, {90, "Tarawa"})
Menu1116 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Check for steaming into wind", tarawa_menu, checkSteamingIntoWind, "Tarawa")
Menu1117 = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Get remaining time for recovery", tarawa_menu, getTimeRemaining, "Tarawa")

-- Level 3 Beacons Tarawa
tarawa_tacan = MENU_COALITION:New(coalition.side.BLUE, "TACAN" , tarawa_beacons) -- Menu1121
tarawa_icls = MENU_COALITION:New(coalition.side.BLUE, "ICLS" , tarawa_beacons) -- Menu 1122

-- Level 3 Lights Tarawa
tarawa_lights_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Turn Off CVN Lights", tarawa_lights, beaconsLightsFlag, {tarawa_flags, "LIGHTS", "OFF"})
tarawa_lights_auto = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights to Auto", tarawa_lights, beaconsLightsFlag, {tarawa_flags, "LIGHTS", "AUTO"})
tarawa_lights_navigation = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights to Navigation", tarawa_lights, beaconsLightsFlag, {tarawa_flags, "LIGHTS", "NAVIGATION"})
tarawa_lights_launch = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights for Launch", tarawa_lights, beaconsLightsFlag, {tarawa_flags, "LIGHTS", "LAUNCH"})
tarawa_lights_reconvery = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Set CVN Lights for Recovery", tarawa_lights, beaconsLightsFlag, {tarawa_flags, "LIGHTS", "RECOVERY"})

--Level 4 Tarawa
tarawa_tacan_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate TACAN", tarawa_tacan, beaconsLightsFlag, {tarawa_flags, "TCN", "OFF"})
tarawa_tacan_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart TACAN", tarawa_tacan, beaconsLightsFlag, {tarawa_flags, "TCN", "ON"})
tarawa_icls_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Deactivate ICLS", tarawa_icls, beaconsLightsFlag, {tarawa_flags, "ICLS", "OFF"})
tarawa_icls_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE,"Restart ICLS", tarawa_icls, beaconsLightsFlag, {tarawa_flags, "ICLS", "ON"})

-- Tarawa END
]]--

MESSAGE:New("Finished loading Airboss Script. Mission Version : V4.6.1 . Script Version : v4.2", 10, "END"):ToAll()