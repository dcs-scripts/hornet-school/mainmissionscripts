# Main Mission V4.6
(Last update unless something breaks from DCS for the caucasus)

## Common
1. Only hornets remain on the server, other planes have been removed for now.

## Training Sector
1. GWA group has been reduced to three ships includinfg the carrier. Also the base track has been changed to prevent it from going into the Free Flight Sector.
2. KC-130 height has been changed to 15000ft.
3. Krymsk CQ planes are now visible on the map.

## Free Flight Sector
1. Dynamic Spawn points have been removed. Will be added in a later update.
2. The Stennis Group has been removed.


The main mission is being remade from scratch so a lot of things have been removed in this update to reflect what may come there.