MESSAGE:New("Loading Random Air Traffic. Version 1.6.5 . Mission Version 4.5.1", 10, "INFO"):ToAll()

-----------------------------------------------------------------------------------------------------------------------------------------
-- Civilian Traffic
-----------------------------------------------------------------------------------------------------------------------------------------

-- Planes list : RAT_An-26B, RAT_An-30M, RAT_B-17G, RAT_C-47, RAT_IL-76MD, RAT_Yak-40

local an26b = RAT:New("RAT_AN-26B")
an26b:SetCoalitionAircraft("neutral")
an26b:ContinueJourney()
an26b:SetMinDistance(100)
an26b:SetFLmin(90)
an26b:SetFLmax(100)
an26b:SetEPLRS(true)
an26b:RadioOFF()
an26b:ATC_Messages(false)
an26b:Spawn(3)
-- an26b:SetTakeoff("air")

local il76 = RAT:New("RAT_IL76")
il76:SetCoalitionAircraft("neutral")
il76:ContinueJourney()
il76:SetMinDistance(100)
il76:SetFLmin(200)
il76:SetEPLRS(true)
il76:SetFLmax(300)
il76:RadioOFF()
il76:ATC_Messages(false)
il76:Spawn(3)
-- il76:SetTakeoff("air")

local f117 = RAT:New("RAT_F117")
f117:SetCoalitionAircraft("neutral")
f117:ContinueJourney()
f117:SetMinDistance(100)
f117:SetFLmin(200)
f117:SetFLmax(300)
f117:SetEPLRS(true)
f117:RadioOFF()
f117:ATC_Messages(false)
f117:Spawn(2)
-- yak40:SetTakeoff("air")

-----------------------------------------------------------------------------------------------------------------------------------------
-- Blue Traffic
-----------------------------------------------------------------------------------------------------------------------------------------

-- Planes List : RAT_B52H, RAT-F4E

local b1b = RAT:New("RAT_B1B")
b1b:SetFLmin(30)
b1b:SetFLmax(40)
b1b:SetMinDistance(100)
b1b:SetCoalitionAircraft("blue")
b1b:ContinueJourney()
b1b:SetDeparture({"Tbilisi-Lochini", "Maykop-Khanskaya", "Krymsk", "Kutaisi"})
b1b:SetDestination({"Tbilisi-Lochini", "Maykop-Khanskaya", "Krymsk", "Kutaisi"})
b1b:SetTerminalType(AIRBASE.TerminalType.OpenBig)
b1b:SetTakeoff("hot")
b1b:SetEPLRS(true)
b1b:RadioOFF()
b1b:ATC_Messages(false)
b1b:Spawn(3)

-- local f4e = RAT:New("RAT-F4E")
-- f4e:SetFLmin(250)
-- f4e:SetFLmax(350)
-- f4e:SetMinDistance(100)
-- f4e:SetCoalitionAircraft("blue")
-- f4e:ContinueJourney()
-- f4e:SetDeparture({"Tbilisi-Lochini", "Maykop-Khanskaya", "Krymsk", "Kutaisi"})
-- f4e:SetDestination({"Tbilisi-Lochini", "Maykop-Khanskaya", "Krymsk", "Kutaisi"})
-- -- f4e:SetTerminalType(AIRBASE.TerminalType.OpenBig)
-- f4e:SetTakeoff("hot")
-- f4e:SetEPLRS(true)
-- f4e:Spawn(3)


MESSAGE:New("Loading Random Air Traffic. Version 1.6.5 . Mission Version 4.5.1", 10, "END"):ToAll()