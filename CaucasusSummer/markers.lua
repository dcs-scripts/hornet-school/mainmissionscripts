-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Placing Map Markers. Mission Version : v4.5.1 ; Script Version : v1.0", 10, "INFO"):ToAll()

---------------------------------------------
-- Krasnodar Markers
---------------------------------------------

local kras_coords = AIRBASE:FindByName("Krasnodar-Pashkovsky"):GetCoordinate()
kras_marker = MARKER:New(kras_coords, "Krasnodar-Pashkovsky, active runway 05R BRC : 040, \nTCN 36X KRP, \nCTAF : 122.800, \nAI ATC : 257.000"):ReadOnly():ToAll()

local kras_atc_coord = COORDINATE:New(00008994, 0, 00387454)
kras_atc_marker = MARKER:New(kras_atc_coord, "Tower : 119.900 SFC-5000MSL,\nApproach : 125.500, \nBackup TCN 37X BKP"):ReadOnly():ToAll()

-- Departure Markers
local north_dep_coords = COORDINATE:New(00024824, 0, 00395160)
north_dep_marker = MARKER:New(north_dep_coords, "NORTH DEPARTURE"):ReadOnly():ToAll()

local point_adam_coords = COORDINATE:New(00014552, 0, 00408644)
point_adam_marker = MARKER:New(point_adam_coords, "POINT ADAM"):ReadOnly():ToAll()

local adam_dep_coords = COORDINATE:New(00006673, 0, 00419828)
adam_dep_marker = MARKER:New(adam_dep_coords, "ADAM DEPARTURE"):ReadOnly():ToAll()

-- Reference Markers
local x_coords = COORDINATE:New(00024430, 0, 00416400)
x_marker = MARKER:New(x_coords, "THE X"):ReadOnly():ToAll()

local elbow_coords = COORDINATE:New(-00006870, 0, 00388222)
elbow_marker = MARKER:New(elbow_coords, "ELBOW"):ReadOnly():ToAll()

local reservoir_coords = COORDINATE:New(-00001416, 0, 00360134)
reservoir_marker = MARKER:New(reservoir_coords, "RESERVOIR"):ReadOnly():ToAll()

local kras_centre_coords = COORDINATE:New(00011280, 0, 00368500)
kras_centre_marker = MARKER:New(kras_centre_coords, "KRASNODAR CENTRE"):ReadOnly():ToAll()

-- Approach Marker
local river_coords = COORDINATE:New(00003765, 0, 00372254)
river_marker = MARKER:New(river_coords, "RIVER"):ReadOnly():ToAll()

local bridge_coords = COORDINATE:New(00002402, 0, 00382192)
bridge_marker = MARKER:New(bridge_coords, "BRIDGE (Initial)"):ReadOnly():ToAll()

-- Maykop Markers
local maykop_coords = AIRBASE:FindByName("Maykop-Khanskaya"):GetCoordinate()
maykop_marker = MARKER:New(maykop_coords, "Maykop, active runway 04 BRC : 031, \nTCN 42X MAY, \nCTAF : 122.800, \nAI ATC : 254.000"):ReadOnly():ToAll()

-- Mineralyne-Vody
local mineral_coords = AIRBASE:FindByName("Mineralnye Vody"):GetCoordinate()
mineral_marker = MARKER:New(mineral_coords, "Mineralnye, actice runway 12 BRC : 108, \nTCN 38X MIN, \nCTAF : 122.800, \nAI ATC : 264.000"):ReadOnly():ToAll()

-- Mozdok
local mozdok_coords = AIRBASE:FindByName("Mozdok"):GetCoordinate()
mozdok_marker = MARKER:New(mozdok_coords, "Mozdok, active runway 08 BRC : 075, \nTCN 34X MOZ, \nCTAF : 122.800, \nAI ATC : 266.000"):ReadOnly():ToAll()

MESSAGE:New("Placing Map Markers. Mission Version : v4.5.1 ; Script Version : v1.0", 10, "END"):ToAll()