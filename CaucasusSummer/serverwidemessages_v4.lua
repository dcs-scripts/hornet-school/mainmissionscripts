-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Loading messages script. Mission Version : v4.5.1 . Script Version : v1.3.3", 5, "INFO"):ToAll()

function serverReset(time_reset)
    local timenow=timer.getAbsTime( )
    local timeend = timenow + time_reset*60

    timeend = UTILS.SecondsToClock(timeend,false)
    MESSAGE:New("Server will restart in "..time_reset.." at "..timeend, 60, "INFO"):ToAll()
end

function serverRestartTimeLeft()
    local time_now = timer.getTime()
    local time_left = 46800 - time_now

    -- MESSAGE:New("Time left in seconds : "..time_left, 15, "INFO"):ToAll()
    -- MESSAGE:New("Time left datatype "..type(time_left), 15, "INFO"):ToAll()

    local time_left_hours = math.floor(time_left / 3600)
    local time_left_min = math.floor(time_left % 3600 / 60)
    local time_left_sec = math.floor(time_left % 3600 % 60)

    MESSAGE:New("Time till server restart "..time_left_hours..":"..time_left_min..":"..time_left_sec, 15,"INFO"):ToAll()
end

-----------------------------------------------------------------------------------------------------------------------------------------
-- Restart Airfield TACANs
-----------------------------------------------------------------------------------------------------------------------------------------

airfield_messages = {KRP_TCN = {ON="KRP TACAN Restarted", OFF="KRP TACAN Deactivated. Click on the corresponding activate option."},
                KRP_BKP_TCN = {ON="KRP BKP TACAN Restarted", OFF="KRP BKP TACAN Deactivated. Click on the corresponding activate option."},
                MIN_TCN = {ON="MIN TCN Restarted", OFF="MIN TCN Deactivated. Click on the corresponding activate option."},
                MOZ_TCN = {ON="MOZ TCN Restarted", OFF="MOZ TCN Deactivated. Click on the corresponding activate option."},
                MAY_TCN = {ON="MAY TCN Restarted", OFF="MAY TCN Deactivated. Click on the corresponding activate option."}
                }

airfield_flags = {KRP_TCN = {ON="61", OFF="60"},
                  KRP_BKP_TCN = {ON="63", OFF="62"},
                  MIN_TCN = {ON="65", OFF="64"},
                  MOZ_TCN = {ON="67", OFF="66"},
                  MAY_TCN = {ON="69", OFF="68"},
                  MESSAGES = airfield_messages}

function orion_beaconsAirfield(airfield_dict_beacon_flag)

   local airfield_tcn  = airfield_dict_beacon_flag[1]
   local airfield_flag = airfield_dict_beacon_flag[2]

   -- local beacon_off = USERFLAG:New(airfield_flags[airfield_tcn]["OFF"]):Set(true)

   local beacon_on = USERFLAG:New(airfield_flags[airfield_tcn][airfield_flag]):Set(true)

   MESSAGE:New(airfield_flags["MESSAGES"][airfield_tcn][airfield_flag], 5, "INFO"):ToAll()
end

-----------------------------------------------------------------------------------------------------------------------------------------
-- GWA Carrier Quals
-----------------------------------------------------------------------------------------------------------------------------------------

gwa_flags = {GWA_CQ_ON = "200",
             GWA_CQ_OFF = "201"}

function gwaCQFlag(gwa_cq_flag)

    local cq_flag = gwa_flags[gwa_cq_flag[1]]

    USERFLAG:New(cq_flag):Set(true)
end 

-- Main Menu
server_messages_menu = MENU_COALITION:New(coalition.side.BLUE, "Server Message")

-- Level 1
time_left_restart_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Time till restart", server_messages_menu, serverRestartTimeLeft)

-- TACAN Restart messages

tacan_menu = MENU_COALITION:New(coalition.side.BLUE, "TACAN", server_messages_menu)

krp_tcn_menu = MENU_COALITION:New(coalition.side.BLUE, "KRP TACAN", tacan_menu)
krp_tcn_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "KRP TACAN Deactivate", krp_tcn_menu, orion_beaconsAirfield, {"KRP_TCN", "OFF"})
krp_tcn_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "KRP TACAN Activate", krp_tcn_menu, orion_beaconsAirfield, {"KRP_TCN", "ON"})

krp_bkp_tcn_menu = MENU_COALITION:New(coalition.side.BLUE, "KRP Backup TACAN", tacan_menu)
krp_bk_tcn_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "KRP Backup TACAN Deactivate", krp_bkp_tcn_menu, orion_beaconsAirfield, {"KRP_BKP_TCN", "OFF"})
krp_bk_tcn_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "KRP Backup TACAN Activate", krp_bkp_tcn_menu, orion_beaconsAirfield, {"KRP_BKP_TCN", "ON"})

min_tcn_menu = MENU_COALITION:New(coalition.side.BLUE, "MIN TACAN", tacan_menu)
min_tcn_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "MIN TACAN Deactivate", min_tcn_menu, orion_beaconsAirfield, {"MIN_TCN", "OFF"})
min_tcn_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "MIN TACAN Activate", min_tcn_menu, orion_beaconsAirfield, {"MIN_TCN", "ON"})

moz_tcn_menu = MENU_COALITION:New(coalition.side.BLUE, "MOZ TACAN", tacan_menu)
moz_tcn_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "MOZ TACAN Deactivate", moz_tcn_menu, orion_beaconsAirfield, {"MOZ_TCN", "OFF"})
moz_tcn_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "MOZ TACAN Activate", moz_tcn_menu, orion_beaconsAirfield, {"MOZ_TCN", "ON"})

may_tcn_menu = MENU_COALITION:New(coalition.side.BLUE, "MAY TACAN", tacan_menu)
may_tcn_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "MAY TACAN Deactivate", may_tcn_menu, orion_beaconsAirfield, {"MAY_TCN", "OFF"})
may_tcn_on = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "MAY TACAN Activate", may_tcn_menu, orion_beaconsAirfield, {"MAY_TCN", "ON"})

-- GWA Carrier Quals Menu

gwa_cq_menu = MENU_COALITION:New(coalition.side.BLUE, "GWA Carrier Quals", server_messages_menu)

gwa_cq_on_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "CQ In Progress", gwa_cq_menu, gwaCQFlag, {"GWA_CQ_ON"})
gwa_cq_off_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "CQ Finished", gwa_cq_menu, gwaCQFlag, {"GWA_CQ_OFF"})

MESSAGE:New("Finished loading messages script. Mission Version : v4.5.1 . Script Version : v1.3.3", 5, "END"):ToAll()