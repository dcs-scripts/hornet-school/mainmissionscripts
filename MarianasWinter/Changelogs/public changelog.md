# Welcome to the Marianas

The hornet school has decided to shift to the Marianas Islands map for a change of pace. The change has also allowed us to add some new things to the mission to make it more interesting for either a group flight or an individual hornet.

The main airbase is Andersen AFB on the island of Guam with Runway 06L reserved only for trainees.

Mission date is set to 21 June 2024 starting at 0730hrs local. Mission restarts every 11 hours. CASE 1 conditions throughout the mission time.

## Weather

1. With the new DCS update, Light Rain 1 preset was added. This allows for clouds similar to the scattered presets to be present with very sporadic rain columns overpassing any given point for no longer than 20 seconds. 
2. Clear below Angels 15 and above Angels 16. Light rain below Angels 15, with possible snow closer to the clouds.

## ATIS

An automatic ATIS system has been implemented via MOOSE along with a secondary ATIS via voice lines for Andersen AFB. Mentors and trainees should utilise them to know which runway to use and what the weather is like.

## Slots

The primary slots are for Hornets with dynamic spawns available at Andersen AFB for a variety of planes such as F-16, F-15E, F-14B. The hornet is also included in the dynamic spawns. 

## Frequencies

A few of the frequencies have been changed. This [video](https://youtu.be/c3NdO7hl7GM) explains in detail the frequencies for the map. The kneeboard for the radio presets and common flight information also have them listed down. 

## Tankers

There are a total of 5 tankers on the map, 2 KC-135MPRS, 1 KC-130, 1 KC-135 and 1 S3B Tanker. The tacans and frequencies are provided in the kneeboards and briefing.

1. Arco 1-1, KC-135MPRS, and Shell 1-1, KC-130, are stationed around Andersen AFB.
2. Arco 2-1, KC-135MPRS, and Arco 3-1, KC-135, are stationed near Saipan for going to the ranges.
3. Bloodhound 1-1, S3B tanker circling the USS George Washington.

## Carriers

1. There is only one carrier ground on the map, consisting of USS "Warfighter" George Washington and two escort destroyers. This carrier group is only for trainees at L6 and above. Free flight is not allowed.
2. The plane slots on this carrier are password locked.
3. The carrier has password protected Airboss and LSO slots, with comms menu for turning the carrier into the wind for 30/60/90 minutes. In a later version, there will be a command to extend the recovery time by an additional 30 minutes.

## Ranges

1. There are a total of 4 ranges present, 3 afar, Ranges Alpha through Charlie (due to SAM/AAA sites placed down) and Range Delta (unarmed) on Rota island, just a hop away. 
2. Range Alpha through Charlie have MOOSE range implemented with it, it will provide feedback on how well the targeting was. The targets are immortal to allow for an easier experience.
    1. The targets can either be marked with smoke or marked on the map using the Comms Menu. Look for `On the Range` under `Other`.
    2. Range Alpha and Bravo are bombing pits, Range Charlie is a strafing range.
3. Range Delta is unarmed trucks. A total of 11 targets are present, and they respawn when all 11 are destroyed.

## Kneeboards

Kneeboards for the hornet's radio presets, taxi charts for Andersen AFB and Saipan, common flight information and finally VFR approach plates for both Andersen AFB and Saipan.