-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Starting Random Air Traffic. Mission Version : V2.0 . Script Version : V1.0", 10, "INFO"):ToAll()

---------------------------------------------
-- Civilian Traffic
-- Planes : RAT-C130, RAT-CH47D, RAT-Mosquito, RAT-MI8
---------------------------------------------

-- local c130 = RAT:New("RAT-C130")
-- c130:SetCoalitionAircraft("neutral")
-- c130:ContinueJourney()
-- c130:RadioFrequency(252.000)
-- c130:RadioModulation("FM")
-- c130:RadioON()
-- -- c130:SetMinDistance(80)
-- c130:SetEPLRS(true)
-- c130:SetFLmin(200)
-- c130:SetDeparture({"Andersen AFB", "Saipan Intl"})
-- c130:SetDestination({"Andersen AFB", "Saipan Intl"})
-- -- c130:ExcludedAirports("Pagan Airstrip")
-- c130:Spawn(3)

local mosquito = RAT:New("RAT-Mosquito")
mosquito:SetCoalitionAircraft("neutral")
mosquito:ContinueJourney()
mosquito:SetMinDistance(80)
mosquito:SetFLmin(10)
-- mosquito:ExcludedAirports("Pagan Airstrip")
mosquito:ATC_Messages(false)
mosquito:Spawn(1)

local ch47d = RAT:New("RAT-CH47D")
ch47d:SetCoalitionAircraft("neutral")
ch47d:ContinueJourney()
ch47d:RadioFrequency(30.000)
ch47d:RadioModulation("FM")
ch47d:RadioON()
ch47d:SetMinDistance(80)
ch47d:SetEPLRS(true)
ch47d:SetFLcruise(30)
ch47d:ATC_Messages(false)
ch47d:Spawn(1)

local mi26 = RAT:New("RAT-MI26")
mi26:SetCoalitionAircraft("neutral")
mi26:ContinueJourney()
mi26:RadioFrequency(30.000)
mi26:RadioModulation("FM")
mi26:RadioON()
mi26:SetMinDistance(80)
mi26:SetEPLRS(true)
mi26:SetFLcruise(30)
mi26:ATC_Messages(false)
mi26:Spawn(1)

---------------------------------------------
-- Blue Traffic
-- Planes : RAT-UH60A, RAT-B1B
---------------------------------------------

local uh60a = RAT:New("RAT-UH60A")
uh60a:SetCoalitionAircraft("blue")
uh60a:SetCoalition("sameonly")
uh60a:SetFLcruise(50)
uh60a:RadioFrequency(30.000)
uh60a:RadioModulation("FM")
uh60a:SetDeparture({"Olf Orote", "North West Field"})
uh60a:SetDestination({"Olf Orote", "North West Field"})
uh60a:SetTakeoff("hot")
uh60a:ATC_Messages(false)
uh60a:ContinueJourney()
uh60a:Spawn(2)

local b1b = RAT:New("RAT-B1B")
b1b:SetCoalitionAircraft("blue")
b1b:SetCoalition("sameonly")
b1b:RadioFrequency(30.000)
b1b:RadioModulation("FM")
b1b:SetFLmin(1)
b1b:SetFLmax(4)
b1b:SetDeparture({"Andersen AFB"})
b1b:SetDestination({"RAT-Zone-B", "RAT-Zone-C", "RAT-Zone-D"})
b1b:SetTakeoff("hot")
b1b:ATC_Messages(false)
b1b:Spawn(2)

MESSAGE:New("Finished loading random air traffic. Mission Version : V2.0 . Script Version : V1.0", 10, "END"):ToAll()