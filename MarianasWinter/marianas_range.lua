-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Initialising Ranges. Mission Version : V2.0 . Script Version : V1.0.3", 5, "INFO"):ToAll()

---------------------------------------------
-- Range Alpha

-- Targets : Range-Alpha-1-1=>6, Range-Alpha-2-1=>6, Range-Alpha-3, Range-Alpha-4
---------------------------------------------

local range_alpha_targets = {"Range-Alpha-1-1", "Range-Alpha-1-2", "Range-Alpha-1-3", "Range-Alpha-1-4", "Range-Alpha-1-5", "Range-Alpha-1-6", "Range-Alpha-2-1", "Range-Alpha-2-2", "Range-Alpha-2-3", "Range-Alpha-2-4", "Range-Alpha-2-5", "Range-Alpha-2-6", "Range-Alpha-3", "Range-Alpha-4"}

range_alpha = RANGE:New("range Alpha")

range_alpha:AddBombingTargets(range_alpha_targets, 20)

range_alpha:SetRangeRadius(5)
range_alpha:SetBombtrackThreshold(40)
range_alpha:SetRangeRadius(40)
range_alpha:SetMessageTimeDuration(10)
range_alpha:SetScoreBombDistance(500)
range_alpha:SetDefaultPlayerSmokeBomb(false)

range_alpha:Start()

MESSAGE:New("Range Alpha initialised", 10, "INFO"):ToAll()

---------------------------------------------
-- Range Bravo

-- Targets : Range-Bravo-1-1=>5, Range-Bravo-2-1=>6, Range-Bravo-3, Range-Bravo-4
---------------------------------------------

local range_bravo_targets = {"Range-Bravo-1-1", "Range-Bravo-1-2", "Range-Bravo-1-3", "Range-Bravo-1-4", "Range-Bravo-1-5", "Range-Bravo-2-1", "Range-Bravo-2-2", "Range-Bravo-2-3", "Range-Bravo-2-4", "Range-Bravo-2-5", "Range-Bravo-2-6", "Range-Bravo-3", "Range-Bravo-4"}

range_bravo = RANGE:New("Range Bravo")

range_bravo:AddBombingTargets(range_bravo_targets, 15)

range_bravo:SetRangeRadius(5)
range_bravo:SetBombtrackThreshold(40)
range_bravo:SetRangeRadius(40)
range_bravo:SetMessageTimeDuration(10)
range_bravo:SetScoreBombDistance(500)
range_bravo:SetDefaultPlayerSmokeBomb(false)
range_bravo:TrackRocketsOFF()

range_bravo:Start()

MESSAGE:New("Range Bravo Initialised", 10, "INFO"):ToAll()

---------------------------------------------
-- Range Charlie

-- Targets : Range-Charlie-1, Range-Charlie-2, Range-Charlie-3, Range-Charlie-4
---------------------------------------------

range_charlie = RANGE:New("Range Charlie")
zone_range_charlie = ZONE:New("Range-Charlie-Zone")
range_charlie:SetRangeZone(zone_range_charlie)

local range_charlie_sp_1 = {"Range-Charlie-1-1", "Range-Charlie-1-2"}
local range_charlie_sp_1_fl = range_charlie:GetFoullineDistance("Range-Charlie-1-1", "Range-Charlie-1-2")
range_charlie:AddStrafePit(range_charlie_sp_1, 7000, 500, 270, false, 20, range_charlie_sp_1_fl)

local range_charlie_sp_2 = {"Range-Charlie-2-1", "Range-Charlie-2-2"}
local range_charlie_sp_2_fl = range_charlie:GetFoullineDistance("Range-Charlie-2-1", "Range-Charlie-2-2")
range_charlie:AddStrafePit(range_charlie_sp_2, 7000, 500, 270, false, 20, range_charlie_sp_2_fl)

local range_charlie_sp_3 = {"Range-Charlie-3-1", "Range-Charlie-3-2"}
local range_charlie_sp_3_fl = range_charlie:GetFoullineDistance("Range-Charlie-3-1", "Range-Charlie-3-2")
range_charlie:AddStrafePit(range_charlie_sp_3, 7000, 500, 270, false, 20, range_charlie_sp_3_fl)

local range_charlie_sp_4 = {"Range-Charlie-4-1", "Range-Charlie-4-2"}
local range_charlie_sp_4_fl = range_charlie:GetFoullineDistance("Range-Charlie-4-1", "Range-Charlie-4-2")
range_charlie:AddStrafePit(range_charlie_sp_4, 7000, 500, 270, false, 20, range_charlie_sp_4_fl)

range_charlie:TrackRocketsOFF()
range_charlie:SetMaxStrafeAlt(3000)
range_charlie:SetMessageTimeDuration(10)

range_charlie:Start()

MESSAGE:New("Range Charlie Initialised.", 10, "INFO"):ToAll()

MESSAGE:New("Initialised all ranges. Mission Version : V2.0 . Script Version : V1.0.3", 5, "END"):ToAll()