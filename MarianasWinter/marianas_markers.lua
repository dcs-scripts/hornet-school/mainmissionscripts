-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Placing map markers. Mission Version : v2.0; Script Version : v0.4.2", 10, "INFO"):ToAll()

---------------------------------------------
-- BLUE Airports
---------------------------------------------

---------------------------------------------
-- Andersen AFB markers
---------------------------------------------

local andersen_afb_coordinate = AIRBASE:FindByName("Andersen AFB"):GetCoordinate()
andersen_afb_inform_marker = MARKER:New(andersen_afb_coordinate, "Andersen AFB, active runway 06 R/L, \nTCN 54X UAM, \nCTAF: 253.800/140.800 MHz, \nAI ATC: 250.100/126.200 MHz,  \nall aircraft"):ReadOnly():ToAll()

local andersen_atc_coord = COORDINATE:New(00012161, 0, 00014470):GetCoordinate()
andersen_atc_marker = MARKER:New(andersen_atc_coord, "Andersen Approach : 251.100 MHz, \nAndersen Tower : 251.200 MHz, \nAndersen Ground : 251.250 MHz, \nATIS Frequency : 251.500 MHz AM, \nMako Broadcast : 251.600 MHz AM,"):ReadOnly():ToAll()

---------------------------------------------
-- Saipan Intl markers
---------------------------------------------

local saipan_coord = AIRBASE:FindByName("Saipan Intl"):GetCoordinate()
saipan_inform_marker = MARKER:New(saipan_coord, "Saipan Intl, active runway 07/09, \nTCN 47X SAI, \nCTAF: 254.800/141.800 MHz, \nAI ATC: 256.900/125.700 MHz, \nAll aircraft, Rearming and Refuel"):ReadOnly():ToAll()

local saipan_atc_coord = COORDINATE:New(00181171, 0, 00101451)
saipan_atc_marker = MARKER:New(saipan_atc_coord, "Saipan Approach : 257.100 MHz, \nSaipan Tower : 257.200 MHz, \nSaipan Ground : 257.250 MHz"):ReadOnly():ToAll()

-- Olf Orote
local olf_orote_coords = AIRBASE:FindByName("Olf Orote"):GetCoordinate()
olf_orote_marker = MARKER:New(olf_orote_coords, "Olf Orote, CTAF : 40.5 MHz FM, \nHelicopter base"):ReadOnly():ToAll()

-- North West Field
local nw_coords = AIRBASE:FindByName("North West Field"):GetCoordinate()
nw_marker = MARKER:New(nw_coords, "North West Field, CTAF : 140.800 MHz, \nWW2 Base, \nDo not land in red marked area."):ReadOnly():ToAll()

---------------------------------------------
-- NEUTRAL Airports
---------------------------------------------

-- Tinian Intl
local tinian_coord = AIRBASE:FindByName("Tinian Intl"):GetCoordinate()
tinian_inform_marker = MARKER:New(tinian_coord, "NEUTRAL AIRFIELD"):ReadOnly():ToAll()

-- Antonio B. Won Pat Intl
local antonio_coord = AIRBASE:FindByName("Antonio B. Won Pat Intl"):GetCoordinate()
antonio_inform_marker = MARKER:New(antonio_coord, "NEUTRAL AIRFIELD"):ReadOnly():ToAll()

-- Pagan Airstrip
local pagan_coord = AIRBASE:FindByName("Pagan Airstrip"):GetCoordinate()
pagan_inform_marker = MARKER:New(pagan_coord, "NEUTRAL AIRFIELD"):ReadOnly():ToAll()

-- Rota Intl
local rota_coord = AIRBASE:FindByName("Rota Intl"):GetCoordinate()
rota_inform_marker = MARKER:New(rota_coord, "FRIENDLY AIRFIELD"):ReadOnly():ToAll()

MESSAGE:New("Placing map markers. Mission Version : v2.0; Script Version : v0.4.2", 10, "END"):ToAll()