-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Starting recovery tanker and carrier AWACS. Mission Version : V2.0 . Script Version : V1.2.3", 10, "INFO"):ToAll()

---------------------------------------------
-- GWA S3B Tanker Bloodhound 1-1

-- TACAN : 120Y B11
-- Frequency : 278.100 MHz
-- Altitude : 17000 feet
-- Speed : 300 knts TAS
-- Distbow : 15nm Diststern : 10nm
---------------------------------------------

bloodhound_gwa = RECOVERYTANKER:New("GeorgeWashington", "S3B-Tanker")
bloodhound_gwa:SetCallsign(6, 1)
bloodhound_gwa:SetTakeoffHot()
bloodhound_gwa:SetTACAN(120, "B11", "Y")
bloodhound_gwa:SetRadio(278.100, "FM")
bloodhound_gwa:SetSpeed(300)
bloodhound_gwa:SetAltitude(13000)
-- bloodhound_gwa:SetLowFuelThreshold(95)
bloodhound_gwa:SetUnlimitedFuel(true)
bloodhound_gwa:SetRacetrackDistances(15, 20)
bloodhound_gwa:Start()

---------------------------------------------
-- GWA Awacs Darkstar 1-1

-- Frequency : 
-- Altitude : 30000 feet
-- Speed : 350 knts TAS
-- Distbow : 25nm Diststern : 20nm
---------------------------------------------

darkstar_gwa = RECOVERYTANKER:New("GeorgeWashington", "E2D-Carrier")
darkstar_gwa:SetCallsign(5, 1)
-- darkstar_gwa:SetTACAN(125, "D11", "Y")
darkstar_gwa:SetTACANoff()
darkstar_gwa:SetTakeoffCold()
darkstar_gwa:SetAWACS()
darkstar_gwa:SetRadio(230.100, "FM")
darkstar_gwa:SetSpeed(350)
darkstar_gwa:SetAltitude(20000)
darkstar_gwa:SetRacetrackDistances(30, 25)
darkstar_gwa:SetUnlimitedFuel(true)
darkstar_gwa:Start()

MESSAGE:New("Finished loading recovery tankers and carrier awacs. Mission Version : V2.0 . Script Version : V1.2.3", 10, "END"):ToAll()