-- No MOOSE settings menu. Comment out this line if required.
_SETTINGS:SetPlayerMenuOff()

MESSAGE:New("Loading messages script. Mission Version : v1.2 . Script Version : v0.3.8", 5, "INFO"):ToAll()

function serverRestartTimeLeft()
    local time_now = timer.getTime()
    local time_left =  - time_now

    -- MESSAGE:New("Time left in seconds : "..time_left, 15, "INFO"):ToAll()
    -- MESSAGE:New("Time left datatype "..type(time_left), 15, "INFO"):ToAll()

    local time_left_hours = math.floor(time_left / 3600)
    local time_left_min = math.floor(time_left % 3600 / 60)
    local time_left_sec = math.floor(time_left % 3600 % 60)

    MESSAGE:New("Time till server restart "..time_left_hours..":"..time_left_min..":"..time_left_sec, 15,"INFO"):ToAll()
end

airfield_messages = {SAI_TCN = {OFF="Saipan TACAN Deactivated", ON="Saipan TACAN Restarted"}}

airfield_flags = {SAI_TCN = {OFF="20", ON="21"},
                  MESSAGES = airfield_messages}

function beaconsAirfield(airfield_dict_beacon_flag)

    local airfield_tcn = airfield_dict_beacon_flag[1]
    local airfield_flag = airfield_dict_beacon_flag[2]

    local beacon_on = USERFLAG:New(airfield_flags[airfield_tcn][airfield_flag]):Set(true)

    MESSAGE:New(airfield_flags["MESSAGES"][airfield_tcn][airfield_flag], 5, "INFO"):ToAll()
end

client_instance = CLIENTWATCH:New({"Anderson", "Andersen", "QA", "Training"})
-- client_sound = USERSOUND:New("Introduction.wav")

function client_instance:OnAfterSpawn(From,Event,To,ClientObject,EventData)

    local player_unit = ClientObject.Unit
    local player_name = ClientObject.PlayerName
    local player_client = ClientObject.Client

    local welcome_message = player_name .. ", Welcome to the The Hornet School Training Map. Please read the rules in the mission briefing and common flight info kneeboard. Kneeboards are provided for common information and radio presets in the hornets. For Andersen AFB, Runway 06L is reserved for trainees only."

    MESSAGE:New(welcome_message, 10):ToUnit(player_unit)
    -- client_sound:ToClient(player_client)
end

-- Main Menu
server_messages_menu = MENU_COALITION:New(coalition.side.BLUE, "Server Message")

-- Level 1
time_left_restart_menu = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "Time till restart", server_messages_menu, serverRestartTimeLeft)

-- TACAN Restart Commands

tacan_menu = MENU_COALITION:New(coalition.side.BLUE, "TACAN", server_messages_menu)

sai_tcn_menu = MENU_COALITION:New(coalition.side.BLUE, "Saipan TACAN", tacan_menu)
sai_tcn_menu_off = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "SAI TCN Deactivate", sai_tcn_menu, beaconsAirfield, 
                                              {"SAI_TCN", "OFF"})
sai_tcn_menu_on  = MENU_COALITION_COMMAND:New(coalition.side.BLUE, "SAI TCN Restart", sai_tcn_menu, beaconsAirfield, 
                                              {"SAI_TCN", "ON"})

MESSAGE:New("Loading messages script. Mission Version : v1.2 . Script Version : v0.3.8", 5, "END"):ToAll()